﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SchoolReport
{
  /// <summary>
  /// Interaction logic for EnrollForm.xaml
  /// </summary>
  public partial class EnrollForm : UserControl
  {
    public EnrollForm()
    {
      InitializeComponent();
    }

    private void DataGrid_AutoGeneratingColumn_1(object sender, DataGridAutoGeneratingColumnEventArgs e)
    {
      if ((string)e.Column.Header == "Id")
      {
        e.Cancel = true;
      }

    }

  }
}
