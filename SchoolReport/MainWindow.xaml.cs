﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Windows;

namespace SchoolReport
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : INotifyPropertyChanged
  {
    private SchoolReportView schoolReportView;
    public MainWindow()
    {
      InitializeComponent();
      DataContext = this;
      this.schoolReportView = new SchoolReportView();
    }

    internal void Initialize()
    {
        PropertyChanged(this, new PropertyChangedEventArgs("SchoolReportView"));
    }

    public SchoolReportView SchoolReportView { get { return this.schoolReportView; } }
    public event PropertyChangedEventHandler PropertyChanged = delegate { };

  }
}
