﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;
using System.Windows.Controls;
using SchoolReport.Model;
using System.Data.Sql;
using System.Data.SqlClient;

namespace SchoolReport.ViewModel
{
  class SchoolReportViewModel : ObservableObject, IBusinessLogicLayer
  {
    // Exception codes
    private const int DUPLICATE_EXCEPTION_ERROR  = 2601;

    private const string INSERTION_SUCCESS      = "El registro se han guardado satisfactoriamente.";
    private const string INSERTION_ERROR        = "Ha ocurrido un problema al intentar guardar el registro.";
    private const string EMPTY_NAME_ERROR       = "El nombre debe contener al menos un caracter.";
    private const string EMPTY_FIELD_ERROR      = "El campo no puede ser vacio.";
    private const string FIELD_ERROR            = "Algunos campos tienen errores.";
    private const string DUPLICATE_ROW_ERROR    = "El registro ya existe en la base de datos.";
    private const string NONEXISTENT_USER_ERROR = "El usuario especificado no existe.";

    private StudentForm _studentForm;
    private CourseForm _courseForm;
    private EnrollForm _enrollForm;
    private AddOperationMenu _addOperationMenu;
    private AddOperationMenu _deleteOperationMenu;

    private ContentControl _addContentControl;
    private ContentControl _deleteContentControl;
    private ContentControl _updateContentControl;
    private ContentControl _queryContentControl;

    // Ready
    private ICommand _loadStudentForm;
    private ICommand _loadCourseForm;
    private ICommand _loadEnrollForm;

    private ICommand _loadAddOperationMenu;
    private ICommand _loadDeleteOperationMenu;
    private ICommand _loadUpdateOperationMenu;
    private ICommand _loadQueryOperationMenu;

    private ICommand _saveCourse;
    private ICommand _saveStudent;
    private ICommand _searchAvailableCourses;
    private ICommand _saveStudentCourses;

    private Course _course;
    private Student _student;

    private List<DataGridCourse> _availableCourses;
    private String _displayMessage;

    private SchoolDataLayer _schoolDataLayer;
    private int _studentSearchId;

    public SchoolReportViewModel()
    {
      this._studentForm = new StudentForm();
      this._courseForm = new CourseForm();

      this._addContentControl = new AddOperationMenu();
      this._deleteContentControl = new DeleteOperationMenu();
      this._updateContentControl = new UpdateOperationMenu();
      this._queryContentControl = new QueryOperationMenu();

      this._course = new Course();
      this._student = new Student();

      _schoolDataLayer = new SchoolDataLayer();
    }

    public String DisplayMessage
    {
      get
      {
        if (this._displayMessage == null)
          this._displayMessage = String.Empty;
        return this._displayMessage;
      }

      set
      {
        this._displayMessage = value;
        OnPropertyChanged("DisplayMessage");
      }
    }


    public Course Course
    {
      get { return this._course; }
      set
      {
        if (value != this._course) 
        {
          this._course = value;
        }
      }
    }

    public Student Student
    {
      get { return this._student; }
      set
      {
        if (value != this._student)
        {
          this._student = value;
        }
      }
    }
    
    protected abstract class Command : ICommand
    {
      public bool CanExecute(object parameter)
      {
        return true;
      }

      public event EventHandler CanExecuteChanged;

      public abstract void Execute(object parameter);
    }

    #region Add Operations
    #region AddContentControl
    public ContentControl AddContentControl
    {
      get
      {
        return this._addContentControl;
      }

      set
      {
        if (_addContentControl != value)
        {
          _addContentControl = value;
          OnPropertyChanged("AddContentControl");
        }
      }
    }

    public ICommand LoadAddOperationMenu
    {
        get
        {
            if (_loadAddOperationMenu == null)
                _loadAddOperationMenu =  new AddOperationMenuLoader();
            return _loadAddOperationMenu;
        }
    }

    protected class AddOperationMenuLoader: Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.AddContentControl = new AddOperationMenu();
        viewModel.DisplayMessage = String.Empty;
      }
    }
    #endregion

    public ICommand SaveCourse
    {
      get
      {
        if (this._saveCourse == null)
          this._saveCourse = new CourseSaver();
        return this._saveCourse;
      }
    }

    protected class CourseSaver : Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.Insert(viewModel.Course);

      }
    }


    public ICommand SaveStudent
    {
      get
      {
        if (this._saveStudent == null)
          this._saveStudent = new StudentSaver();
        return this._saveStudent;
      }
    }

    protected class StudentSaver : Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.Insert(viewModel.Student);
      }
    }

    public ICommand SearchAvailableCourses
    {
      get
      {
        if (this._searchAvailableCourses == null)
          this._searchAvailableCourses = new SearchCourseCommand();
        return this._searchAvailableCourses;
      }
    }

    protected class SearchCourseCommand : Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        int id = viewModel.StudentSearchId;
        if (viewModel.Read(id) == null) {
          viewModel.DisplayMessage = NONEXISTENT_USER_ERROR;
          viewModel.AvailableCourses = new List<DataGridCourse>();
        } else {
          List<DataGridCourse> list = viewModel.GetAvailableCourses(id);
          viewModel.AvailableCourses = list;
        }
      }
    }

    public ICommand SaveStudentCourses
    {
      get
      {
        if (this._saveStudentCourses == null)
          this._saveStudentCourses = new StudentCourseSaver();
        return this._saveStudentCourses;
      }
    }

    protected class StudentCourseSaver : Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.SaveStudentCourseList(viewModel.StudentSearchId, viewModel.AvailableCourses);
      }
    }

    #endregion

    #region DeleteContentControl
    public ContentControl DeleteContentControl
    {
      get
      {
        return this._deleteContentControl;
      }

      set
      {
        if (_deleteContentControl != value)
        {
          _deleteContentControl = value;
          OnPropertyChanged("DeleteContentControl");
        }
      }
    }

    public ICommand LoadDeleteOperationMenu
    {
        get
        {
            if (_loadDeleteOperationMenu == null)
                _loadDeleteOperationMenu =  new DeleteOperationMenuLoader();
            return _loadDeleteOperationMenu;
        }
    }

    protected class DeleteOperationMenuLoader: Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.DeleteContentControl = new DeleteOperationMenu();
      }
    }
    #endregion


    #region UpdateContentControl
    public ContentControl UpdateContentControl
    {
      get
      {
        return this._updateContentControl;
      }

      set
      {
        if (_updateContentControl != value)
        {
          _updateContentControl = value;
          OnPropertyChanged("UpdateContentControl");
        }
      }
    }

    public ICommand LoadUpdateOperationMenu
    {
        get
        {
            if (_loadUpdateOperationMenu == null)
                _loadUpdateOperationMenu =  new UpdateOperationMenuLoader();
            return _loadUpdateOperationMenu;
        }
    }

    protected class UpdateOperationMenuLoader: Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.UpdateContentControl = new UpdateOperationMenu();
      }
    }
    #endregion


    #region QueryContentControl
    public ContentControl QueryContentControl
    {
      get
      {
        return this._queryContentControl;
      }

      set
      {
        if (_queryContentControl != value)
        {
          _queryContentControl = value;
          OnPropertyChanged("QueryContentControl");
        }
      }
    }

    public ICommand LoadQueryOperationMenu
    {
        get
        {
            if (_loadQueryOperationMenu == null)
                _loadQueryOperationMenu =  new QueryOperationMenuLoader();
            return _loadQueryOperationMenu;
        }
    }

    protected class QueryOperationMenuLoader: Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.QueryContentControl = new QueryOperationMenu();
      }
    }
    #endregion


    #region StudentForm
    public StudentForm StudentForm
    {
      get { return this._studentForm; }
      set
      {
        if (_studentForm != value)
        {
          _studentForm = value;
          OnPropertyChanged("StudentForm");
        }
      }
    }

    public ICommand LoadStudentForm
    {
      get
      {
        if (_loadStudentForm == null)
          _loadStudentForm = new StudentFormLoader();
        return _loadStudentForm;
      }

      set { _loadStudentForm = value; }
    }


    protected class StudentFormLoader: Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.AddContentControl = new StudentForm();
      }
    }
    #endregion

    #region CourseForm
    public CourseForm CourseForm
    {
      get
      {
        if (_courseForm == null)
          _courseForm = new CourseForm();
        return _courseForm;
      }
    }

    public ICommand LoadCourseForm
    {
      get
      {
        if (_loadCourseForm == null)
          _loadCourseForm = new CourseFormLoader();
        return _loadCourseForm;
      }
    }

    protected class CourseFormLoader : Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.AddContentControl = new CourseForm();
      }

    }
    #endregion

    #region EnrollmentForm
    public EnrollForm EnrollForm
    {
      get
      {
        if (_enrollForm == null)
          _enrollForm = new EnrollForm();
        return _enrollForm;
      }
    }

    public ICommand LoadEnrollForm
    {
      get
      {
        if (_loadEnrollForm == null)
          _loadEnrollForm = new EnrollFormLoader();
        return _loadEnrollForm;
      }
    }

    protected class EnrollFormLoader : Command
    {
      public override void Execute(object parameter)
      {
        SchoolReportViewModel viewModel = (SchoolReportViewModel)parameter;
        viewModel.AddContentControl = new EnrollForm();
        viewModel.AvailableCourses = new List<DataGridCourse>();
      }

    }
    #endregion


    // Available courses for enrollment
    #region
    public List<DataGridCourse> AvailableCourses
    {
      get
      {
        if (this._availableCourses == null)
          this._availableCourses = new List<DataGridCourse>();
        return this._availableCourses;
      }

      set
      {
        this._availableCourses = value;
        OnPropertyChanged("AvailableCourses");
      }
    }
    #endregion



    public int Insert(Student student)
    {
      // Validation
      if (student.HasErrors) {
        DisplayMessage = FIELD_ERROR;
        return 0;
      }

      this._student.Name = this._student.Name.Trim();
      this._student.MiddleName = this._student.MiddleName.Trim();
      this._student.LastName = this._student.LastName.Trim();

      if (String.IsNullOrEmpty(student.Name))
      {
        System.Windows.MessageBox.Show(student.Name);
        DisplayMessage = EMPTY_FIELD_ERROR;
        return 0;
      }


      if (String.IsNullOrEmpty(student.LastName))
      {
        System.Windows.MessageBox.Show(student.Name);
        DisplayMessage = EMPTY_FIELD_ERROR;
        return 0;
      }

      // Execute
      string message = String.Empty;
      try {
        int id = _schoolDataLayer.Insert(student);
        message = INSERTION_SUCCESS + " Con id " + id;
        this._student.Name = " ";
        this._student.MiddleName = " ";
        this._student.LastName = " ";
      } catch (SqlException e) {
        if (e.Number == DUPLICATE_EXCEPTION_ERROR)
          message = DUPLICATE_ROW_ERROR;
        else
          message = INSERTION_ERROR;
      }

      // Notify
      DisplayMessage = message;

      return 1;
    }


    public int Insert(Course course)
    {
      // Validation
      if (course.HasErrors) {
        List<String> errors = (List<String>)course.GetErrors("Name");
        if (errors.Any())
          DisplayMessage = errors[0];
        return 0;
      }

      this._course.Name = this._course.Name.Trim();
      if (String.IsNullOrEmpty(course.Name)) {
        DisplayMessage = EMPTY_NAME_ERROR;
        return 0;
      }

      // Execute
      string message = String.Empty;
      try {
        int id = _schoolDataLayer.Insert(course);
        message = INSERTION_SUCCESS + " Con id " + id;
        this._course.Name = " ";
        this._course.Description = String.Empty;
        this._course.ClearErrors();
      } catch (Exception ) {
        message = INSERTION_ERROR;
      }

      // Notify
      DisplayMessage = message;

      return 1;
    }



    public int Insert(Takes takes)
    {
      throw new NotImplementedException();
    }

    public Student Read(int id)
    {
      Student student = null;
      try {
        student = _schoolDataLayer.Read(id);
      } catch(Exception e) {
      }

      return student;
    }

    public Student Read(string name, string middleName, string lastName)
    {
      throw new NotImplementedException();
    }

    public Course Read(string name, string descriptionKeyword)
    {
      throw new NotImplementedException();
    }

    public int Update(int id, Student student)
    {
      throw new NotImplementedException();
    }

    public int Update(int id, Course course)
    {
      throw new NotImplementedException();
    }

    public int Update(int id, Takes takes)
    {
      throw new NotImplementedException();
    }

    public int Delete(int id, Student student)
    {
      throw new NotImplementedException();
    }

    public int Delete(int id, Course course)
    {
      throw new NotImplementedException();
    }

    public int Delete(int id, Takes takes)
    {
      throw new NotImplementedException();
    }

    public override bool HasErrors
    {
      get { return false; }
    }




    public int StudentSearchId
    {
      get { return this._studentSearchId; }

      set
      {
        if (value != this._studentSearchId)
          this._studentSearchId = value;
      }
    }

    public List<DataGridCourse> GetAvailableCourses(int id)
    {
      List<DataGridCourse> list;
      try {
        list = this._schoolDataLayer.GetAvailableCourses(id);
      } catch(Exception e) {
        list = null;
      }

      return list;
    }



    public int SaveStudentCourseList(int id, List<DataGridCourse> courses)
    {
      int count = 0;
      string message = String.Empty;
      try {
        this._schoolDataLayer.SaveStudentCourseList(id, courses);
        count = courses.Count;
        message = INSERTION_SUCCESS;
      } catch (Exception ) {
        message = INSERTION_ERROR;
      }

      DisplayMessage = message;

      return count;
    }

  }



}
