﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolReport.Model;

namespace SchoolReport.ViewModel
{
  public interface IBusinessLogicLayer
  {
    int Insert(Student student);
    int Insert(Course course);
    int Insert(Takes takes);

    Student Read(int id);
    Student Read(string name, string middleName, string lastName);
    Course Read(string name, string descriptionKeyword);
    List<DataGridCourse> GetAvailableCourses(int id);
    int SaveStudentCourseList(int id, List<DataGridCourse> courses);

    int Update(int id, Student student);
    int Update(int id, Course course);
    int Update(int id, Takes takes);

    int Delete(int id, Student student);
    int Delete(int id, Course course);
    int Delete(int id, Takes takes);
  }
}
