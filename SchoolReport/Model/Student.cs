﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Text.RegularExpressions;

namespace SchoolReport.Model
{
  public class Student : ObservableObject
  {
    public static readonly string NAME_ERROR = "Nombre no válido. Se debe comenzar con una letra.";

    private Int32 _id;
    private String _name;
    private String _middleName;
    private String _lastName;


    public Int32 Id
    {
      get { return _id; }
      set
      {
        this._id = value;
        OnPropertyChanged("Id");
      }
    }

    public String Name
    {
      get { return this._name; }
      set
      {
        this._name = value;
        IsNameValid(value, "Name");
        OnPropertyChanged("Name");
      }
    }

    public string MiddleName
    {
      get { return this._middleName; }
      set
      {
        this._middleName = value;
        IsNameValid(value, "MiddleName");
        OnPropertyChanged("MiddleName");
      }
    }

    public string LastName
    {
      get { return this._lastName; }
      set
      {
        this._lastName = value;
        IsNameValid(value, "LastName");
        OnPropertyChanged("LastName");
      }
    }

    public bool IsNameValid(string value, string propertyName)
    {
      bool isValid = true;
      if (!Regex.IsMatch(value, @"^[a-zA-Z ]+"))
      {
        isValid = false;
        AddError(propertyName, NAME_ERROR);
      } else {
        RemoveError(propertyName, NAME_ERROR);
      }

      return isValid;
    }


    public override bool HasErrors
    {
      get { return _errors.Count > 0; }
    }
  }
}
