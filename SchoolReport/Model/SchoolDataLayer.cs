﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace SchoolReport.Model
{
  public class SchoolDataLayer : IDataAbstractionLayer
  {
    DatabaseDataContext database;

    public SchoolDataLayer()
    {
      database = new DatabaseDataContext();
    }


    public int Insert(Course course)
    {
      LINQ_Course c = new LINQ_Course();
      c.Name = course.Name;
      c.Description = course.Description;
      database.LINQ_Courses.InsertOnSubmit(c);
      database.SubmitChanges();

      return c.Id;
    }

    public int Insert(Student student)
    {
      LINQ_Student s = new LINQ_Student();
      s.Name = student.Name;
      s.MiddleName = student.MiddleName;
      s.LastName = student.LastName;
      database.LINQ_Students.InsertOnSubmit(s);
      database.SubmitChanges();

      return s.Id;
    }

    public List<DataGridCourse> GetAvailableCourses(int id)
    {
      var courses = from c in database.LINQ_Courses select c;
      List<DataGridCourse> list = new List<DataGridCourse>();
      foreach (var c in courses)
      {
        DataGridCourse course = new DataGridCourse();
        course.IsSelected = false;
        course.Id = c.Id;
        course.Name = c.Name;
        list.Add(course);
      }

      return list;
    }

    public int SaveStudentCourseList(int id, List<DataGridCourse> courses)
    {
      foreach (var course in courses)
      {
        if (course.IsSelected)
        {
          LINQ_Takes take = new LINQ_Takes();
          take.IdStudent = id;
          take.IdCourse = course.Id;
          database.LINQ_Takes.InsertOnSubmit(take);
        }
      }

      database.SubmitChanges();
      return courses.Count;
    }

    public int Insert(Takes takes)
    {
      throw new NotImplementedException();
    }

    public Student Read(int id)
    {
      var result = from student in database.LINQ_Students where student.Id == id select student;

      foreach (var s in result)
      {
        LINQ_Student linqStudent = (LINQ_Student)s;
        Student student = new Student();
        student.Id = linqStudent.Id;
        student.Name = linqStudent.Name;
        student.MiddleName = linqStudent.MiddleName;
        student.LastName = linqStudent.LastName;
        System.Windows.MessageBox.Show(student.Name);

        return student;
      }

      return null;
    }

    public Student Read(string name, string middleName, string lastName)
    {
      throw new NotImplementedException();
    }

    public Course Read(string name, string descriptionKeyword)
    {
      throw new NotImplementedException();
    }

    public int Update(int id, Student student)
    {
      throw new NotImplementedException();
    }

    public int Update(int id, Course course)
    {
      throw new NotImplementedException();
    }

    public int Update(int id, Takes takes)
    {
      throw new NotImplementedException();
    }

    public int Delete(int id, Student student)
    {
      throw new NotImplementedException();
    }

    public int Delete(int id, Course course)
    {
      throw new NotImplementedException();
    }

    public int Delete(int id, Takes takes)
    {
      throw new NotImplementedException();
    }


  }
}
