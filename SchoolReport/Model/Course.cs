﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace SchoolReport.Model
{
  public class Course : ObservableObject
  {
    public static readonly string NAME_ERROR = "Nombre no válido. Se debe comenzar con una letra.";
    private Int32 _id;
    private String _name;
    private String _description;


    public Course() { }

    public Course(string name, string description)
    {
      Name = name;
      Description = description;
    }

    public Int32 Id
    {
      get { return this._id; }
      set
      {
        this._id = value;
        OnPropertyChanged("Id");
      }
    }

    public String Name
    {
      get { return this._name; }
      set
      {
        if (value != this._name)
        {
          this._name = value;
          IsNameValid(value);
          OnPropertyChanged("Name");
        }
      }
    }

    public bool IsNameValid(string value)
    {
      bool isValid = true;
      if (!Regex.IsMatch(value, @"^[ a-zA-Z0-9]+")) {
        isValid = false;
        AddError("Name", NAME_ERROR);
      } else {
        RemoveError("Name", NAME_ERROR);
      }

      return isValid;
    }


    public String Description
    {
      get { return this._description; }
      set
      {
        this._description = value;
        OnPropertyChanged("Description");
      }
    }


    

    public override bool HasErrors
    {
      get { return _errors.Count > 0; }
    }

  }


  public class DataGridCourse
  {
    public bool IsSelected { get; set; }
    public int Id { get; set; }
    public string Name { get; set; }
  }
}
