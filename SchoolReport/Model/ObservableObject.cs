﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SchoolReport.Model
{
  public abstract class ObservableObject : INotifyPropertyChanged, INotifyDataErrorInfo
  {
    public event PropertyChangedEventHandler PropertyChanged;
    public Dictionary<string, List<string>> _errors;

    public ObservableObject()
    {
      _errors = new Dictionary<string, List<string>>();
    }


    protected virtual void OnPropertyChanged(string propertyName)
    {
      if (this.PropertyChanged != null)
      {
        var e = new PropertyChangedEventArgs(propertyName);
        this.PropertyChanged(this, e);
      }
    }


    public System.Collections.IEnumerable GetErrors(string propertyName)
    {
      if (String.IsNullOrEmpty(propertyName) ||
          !_errors.ContainsKey(propertyName))
        return null;
      return _errors[propertyName];
    }

    abstract public bool HasErrors { get; }


    public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
    public void RaiseErrorsChanged(string propertyName)
    {
      if (ErrorsChanged  != null)
        ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
    }


    public void AddError(string propertyName, string error)
    {
      if (!_errors.ContainsKey(propertyName))
        _errors[propertyName] = new List<string>();

      _errors[propertyName].Add(error);
      RaiseErrorsChanged(propertyName);
    }

    public void RemoveError(string propertyName, string error)
    {
      if (_errors.ContainsKey(propertyName) && _errors[propertyName].Contains(error))
      {
        _errors[propertyName].Remove(error);
        if (_errors[propertyName].Count == 0)
          _errors.Remove(propertyName);
        RaiseErrorsChanged(propertyName);
      }
    }


    public void ClearErrors()
    {
      this._errors.Clear();
    }
  }
}
